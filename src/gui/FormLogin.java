package gui;

import main.Main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import database.Database;

public class FormLogin {

    private JPanel panel, horizPanel;
    private JTextField jtxt_User;
    private JPasswordField pwd_Password;
    private JButton jb_Acceddu;
    private JLabel jlb_user, jlb_password, jlb_effettuaccesso, jlb_alert1, jlb_alert2;

    /*
    Inizializza il pannello ed i componenti della GUI
     */
    public JPanel initialize() {
        panel = new JPanel();
        panel.setBackground(new Color(255,255,255));
        Dimension dim_lbl = new Dimension(300,40);
        Color color_buttonBG = new Color(1,40,100);
        Color color_buttonFG = new Color(246,246,246);
        horizPanel = new JPanel();
        horizPanel.setBackground(new Color(255,255,255));
        horizPanel.setLayout(new BoxLayout(horizPanel, BoxLayout.X_AXIS));

        // LABEL di Titolo
        jlb_effettuaccesso = new JLabel("Registrati con delle credenziali:");
        jlb_effettuaccesso.setHorizontalAlignment(SwingConstants.CENTER);
        jlb_effettuaccesso.setFont(new Font("Verdana", Font.PLAIN, 25));
        jlb_effettuaccesso.setAlignmentX(Component.CENTER_ALIGNMENT);

        // Textbox per il nome utente
        jtxt_User = new JTextField();
        jtxt_User.setText("");
        jtxt_User.setAlignmentX(Component.CENTER_ALIGNMENT);
        jtxt_User.setFont(new Font("Verdana", Font.PLAIN, 20));
        jtxt_User.setMaximumSize(dim_lbl);

        // Textbox per la password
        pwd_Password = new JPasswordField();
        pwd_Password.setAlignmentX(Component.CENTER_ALIGNMENT);
        pwd_Password.setFont(new Font("Verdana", Font.PLAIN, 20));
        pwd_Password.setText("");
        pwd_Password.setMaximumSize(dim_lbl);

        // Button per l'accesso
        jb_Acceddu = new JButton("Registrati");
        jb_Acceddu.setAlignmentX(Component.CENTER_ALIGNMENT);
        jb_Acceddu.setBackground(color_buttonBG);
        jb_Acceddu.setForeground(color_buttonFG);
        jb_Acceddu.setFont(new Font("Verdana", Font.PLAIN, 20));
        jb_Acceddu.setMaximumSize(new Dimension(170,50));

        // Label del textbox user
        jlb_user = new JLabel("Username:");
        jlb_user.setHorizontalAlignment(SwingConstants.LEFT);
        jlb_user.setAlignmentX(Component.CENTER_ALIGNMENT);
        jlb_user.setFont(new Font("Verdana", Font.PLAIN, 20));
        jlb_user.setMaximumSize(dim_lbl);

        // Label del textbox password
        jlb_password = new JLabel("Password:");
        jlb_password.setHorizontalAlignment(SwingConstants.LEFT);
        jlb_password.setAlignmentX(Component.CENTER_ALIGNMENT);
        jlb_password.setFont(new Font("Verdana", Font.PLAIN, 20));
        jlb_password.setMaximumSize(dim_lbl);

        // Mostra errore
        jlb_alert1 = new JLabel("");
        jlb_alert1.setHorizontalAlignment(SwingConstants.LEFT);
        jlb_alert1.setAlignmentX(Component.CENTER_ALIGNMENT);
        jlb_alert1.setFont(new Font("Verdana", Font.PLAIN, 11));
        jlb_alert1.setMaximumSize(new Dimension(300, 15));
        jlb_alert1.setForeground(Color.red);
        jlb_alert1.setVisible(false);

        // Mostra errore
        jlb_alert2 = new JLabel("");
        jlb_alert2.setHorizontalAlignment(SwingConstants.LEFT);
        jlb_alert2.setAlignmentX(Component.CENTER_ALIGNMENT);
        jlb_alert2.setFont(new Font("Verdana", Font.PLAIN, 11));
        jlb_alert2.setMaximumSize(new Dimension(300, 15));
        jlb_alert2.setForeground(Color.red);
        jlb_alert2.setVisible(false);

        // Aggiunge elementi al pannello
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        setSpacer(50);
        panel.add(jlb_effettuaccesso);
        setSpacer(40);
        panel.add(jlb_user);
        panel.add(jtxt_User);
        panel.add(jlb_alert1);
        setSpacer(10);
        panel.add(jlb_password);
        panel.add(pwd_Password);
        panel.add(jlb_alert2);
        setSpacer(35);
        panel.add(jb_Acceddu);

        // Reazione all'accesso
        jb_Acceddu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                doAccedi();
            }
        });

        return panel;
    }

    // Elemento estetico per la divisione degli elementi
    private void setSpacer(int heigth) {
        panel.add(Box.createRigidArea(new Dimension(0, heigth)));
    }

    // Metodo che permette l'accesso e manda a Menumaster
    private void doAccedi() {
        String inserted_user = jtxt_User.getText();
        String inserted_passw = String.valueOf(pwd_Password.getPassword());
        boolean convalidate, passing;
        if (inserted_user.length()!=0 && inserted_passw.length()!=0) {
            convalidate = Database.registrati(inserted_user, inserted_passw);
            if (convalidate) { //CONVALIDATE al posto di true: se il login avviene con successo
                Menumaster _mema = new Menumaster();
                Main.frame.remove(Main.currentPanel);
                Main.currentPanel = _mema.initialize();
                Main.frame.add(Main.currentPanel);
                Main.frame.setVisible(true);
            } else {
                jlb_alert1.setText("Il nome utente è già esistente");
                jlb_alert1.setVisible(true);
                jlb_alert2.setText("Cambia nome utente per effettuare la registrazione");
                jlb_alert2.setVisible(true);
            }
        } else {
            if (inserted_user.length() == 0 && inserted_passw.length() != 0) {
                jlb_alert1.setText("Inserisci il nome utente.");
                jlb_alert1.setVisible(true);
                jlb_alert2.setVisible(false);
            } else if (inserted_passw.length() == 0 && inserted_user.length() != 0) {
                jlb_alert2.setText("Inserisci una password.");
                jlb_alert2.setVisible(true);
                jlb_alert1.setVisible(false);
            } else {
                jlb_alert1.setText("Inserisci il nome utente.");
                jlb_alert1.setVisible(true);
                jlb_alert2.setText("Inserisci una password.");
                jlb_alert2.setVisible(true);
            }
        }
    }

}
