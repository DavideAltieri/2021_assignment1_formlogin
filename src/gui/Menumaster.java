package gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import main.Main;

public class Menumaster {
    private JPanel panel;
    private JLabel jlb_title, jlb_descr, jlb_counter;
    private JButton jb_scappa;

    public Menumaster() {}

    public JPanel initialize() {
        panel = new JPanel();
        panel.setBackground(new Color(255,255,255));

        // Label 1
        jlb_title = new JLabel("Effettuata!");
        jlb_title.setForeground(new Color(1, 40, 100));
        jlb_title.setFont(new Font("Arial black", Font.BOLD, 40));
        jlb_title.setHorizontalAlignment(SwingConstants.CENTER);
        jlb_title.setAlignmentX(Component.CENTER_ALIGNMENT);
        jlb_title.setMaximumSize(new Dimension(800,50));

        // Label 2
        jlb_descr = new JLabel("La registrazione è avvenuta con successo");
        jlb_descr.setHorizontalAlignment(SwingConstants.CENTER);
        jlb_descr.setFont(new Font("Arial", Font.PLAIN, 20));
        jlb_descr.setForeground(new Color(70, 63, 63));
        jlb_descr.setAlignmentX(Component.CENTER_ALIGNMENT);

        //Label con counter
        jlb_counter = new JLabel("");
        jlb_counter.setHorizontalAlignment(SwingConstants.CENTER);
        jlb_counter.setFont(new Font("Arial", Font.PLAIN, 22));
        jlb_counter.setForeground(new Color(70, 63, 63));
        jlb_counter.setAlignmentX(Component.CENTER_ALIGNMENT);
        jlb_counter.setText("Clicca 'Ritorna' per tornare alla schermata");

        // Button per l'accesso
        jb_scappa = new JButton("Ritorna");
        jb_scappa.setAlignmentX(Component.CENTER_ALIGNMENT);
        jb_scappa.setForeground(new Color(1,40,100));
        jb_scappa.setBackground(new Color(246,246,246));
        jb_scappa.setFont(new Font("Verdana", Font.PLAIN, 17));
        jb_scappa.setMaximumSize(new Dimension(130,40));
        jb_scappa.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                backToLogin();
            }
        });

        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        setSpacer(35);
        panel.add(jlb_title);
        setSpacer(50);
        panel.add(jlb_descr);
        setSpacer(50);
        panel.add(jlb_counter);
        setSpacer(80);
        panel.add(jb_scappa);

        return panel;
    }

    // Torna al menu di login
    private void backToLogin() {
        try {
            FormLogin _bto = new FormLogin();
            Main.frame.remove(Main.currentPanel);
            Main.currentPanel = _bto.initialize();
            Main.frame.add(Main.currentPanel);
            Main.frame.setVisible(true);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    // Elemento grafico separatore
    private void setSpacer(int heigth) {
        panel.add(Box.createRigidArea(new Dimension(0, heigth)));
    }

}
