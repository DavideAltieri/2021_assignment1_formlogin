package database;

import java.sql.*;

public class Database {
    private static final String URLPOSTGRES = "jdbc:postgresql://localhost:5432/postgres";
    private static final String URL = "jdbc:postgresql://localhost:5432/assignment1";
    private static final String USER = "postgres";
    private static final String PSW = "postgres";
    private static Connection connection;

    public Database(){}

    public static void initDb(){
        boolean access = accessDb();
        if(!access){
            createDb();
            access = accessDb();
            if(access) {
                try {
                    Statement sqlState = connection.createStatement();
                    sqlState.executeUpdate("create table users (\n" +
                            "	name text not null primary key ,\n" +
                            "	psw text not null\n" +
                            ") ;");
                } catch (SQLException ex) {
                    System.err.println("Assignment1 : Failed to create tables on db \n"+ex);
                }
            }
        }
    }

    private static boolean accessDb(){
        //accedo al DB  creato in precedenza
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(URL, USER, PSW );
            System.err.println("Assignment1 : Connected to "+ connection.getCatalog());
            return true;
        } catch (SQLException | ClassNotFoundException e) {
            System.err.println("Assignment1 : Error the database doesn't exist");
            System.out.println("BSNFBFN");
            return false;
        }
    }

    private static void createDb(){
        try {
            Class.forName("org.postgresql.Driver");
            Connection con = DriverManager.getConnection(URLPOSTGRES, USER, PSW );
            Statement sqlState = con.createStatement();
            String dbCreation ="create database assignment1";
            sqlState.executeUpdate(dbCreation);
            System.err.println("Assignment1 : Created Db  and connected to it ");
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("Assignment1 : Failed to create Db\n"+e);
        }
    }

    public static boolean registrati(String username, String psw) {
        String queryCheck = "select * from users where users.name = ?";
        String queryInsert = "insert into users(name, psw) values(?,?)";
        try {
            PreparedStatement stmt = connection.prepareStatement(queryCheck);
            stmt.setString(1, username);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return false;
            } else {
                stmt = connection.prepareStatement(queryInsert);
                stmt.setString(1, username);
                stmt.setString(2, psw);
                stmt.executeUpdate();
                return true;
            }
        } catch(SQLException e) {e.printStackTrace();}
        return false;
    }

}
