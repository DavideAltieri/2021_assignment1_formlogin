package main;

import gui.FormLogin;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.rmi.RemoteException;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.util.Timer;
import java.util.TimerTask;
import database.Database;

public class Main {
    public static JFrame frame;
    public static JPanel currentPanel;

    public Main() throws RemoteException {
        frame = new JFrame();
        frame.setSize(new Dimension(500, 500));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        Database.initDb();

        setUpLoadingState();


    }

    public static void main(String[] args) {
        try {
            new Main();

        } catch (RemoteException re) { }
    }

    // Metodo per la generazione del frame e dei componenti
    private void setUpLoadingState() {

        JPanel loader_panel =  new JPanel(new BorderLayout());
        loader_panel.add(new JLabel());
        frame.add(loader_panel, BorderLayout.CENTER);
        loader_panel.setVisible(false);
        loader_panel.setVisible(true);

        FormLogin loggui = new FormLogin();
        JPanel tmp_panel = loggui.initialize();

        // Timer per il corretto caricamento della GUI
        Timer timer = new Timer();
        class RemindTask extends TimerTask {
            public void run() {
                System.out.println("App loaded");
                try {
                    frame.remove(loader_panel);
                    currentPanel = tmp_panel;
                    frame.add(currentPanel);
                    currentPanel.setVisible(false);
                    currentPanel.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                timer.cancel();
            }
        }
        timer.schedule(new RemindTask(), 1);
    }

}
